#!/usr/bin/env python
"""Tests for `pyramid_resume_template_default` package."""
# pylint: disable=redefined-outer-name
from pyramid_resume_template_default import __version__


def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.1.0"
