===============================
Pyramid Resume Template Default
===============================

Default template for pyramid_resume

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `opensource/templates/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`opensource/templates/cookiecutter-pypackage`: https://gitlab.com/genomicsengland/opensource/templates/cookiecutter-pypackage
